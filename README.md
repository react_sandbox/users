# Redux + Redux Saga version

## To run

1. Have docker v3.2+ running
2. Run `docker-compose up`

## Routes

-   Home
-   Users - async fetch
    -   test async and global state
-   Me - main state with timer changing + changeable subcomponents
    -   test reconciliation and updating
