import React, { useEffect } from "react";

// Rerender whenever parent changes, even though Person doesn't
// const Person = ({ name }) => {
//     console.log("rendering Person");
//     return <span>{name}</span>;
// };

// Rerender only when name changes
const Person = React.memo(({ name }) => {
    console.log("rendering Person");
    return <span>Name {name}</span>;
});

export default Person;
