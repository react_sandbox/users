import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import styles from "./Users.module.scss";

import User from "./components/User/User";

const Users = props => {
    const dispatch = useDispatch();
    const users = useSelector(state => state);
    useEffect(() => {
        if (Array.isArray(users) && !users.length) {
            dispatch({ type: "USERS_FETCH" });
        }
    }, [users]);

    return (
        <div className={styles.users}>
            {users && users.map(u => <User {...u} key={u.id} />)}
        </div>
    );
};

export default Users;
