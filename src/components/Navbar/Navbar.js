import React from "react";
import { Link } from "react-router-dom";

import styles from "./Navbar.module.scss";

const Navbar = props => (
    <div className={styles.navbar}>
        <Link to="/">Home</Link>
        <Link to="/users">Users</Link>
        <Link to="/me">Me</Link>
    </div>
);

export default Navbar;
