import { call, takeLatest, put } from "redux-saga/effects";
import axios from "axios";

function* fetchUsersWorker() {
    try {
        const { data: users } = yield call(
            axios.get,
            "https://jsonplaceholder.typicode.com/users"
        );
        yield put({ type: "USERS_FETCH_SUCCEEDED", users });
    } catch (e) {
        yield put({ type: "USERS_FETCH_FAILED", message: e.message });
    }
}

function* fetchUsers() {
    yield takeLatest("USERS_FETCH", fetchUsersWorker);
}

export default fetchUsers;
